from uni_helpers import UniHelpers


class UniTransaction(object):
    def __init__(self, time, output_address, input_address, amount, meta_data = None):
        self.time = time
        self.outputAddress = output_address
        self.inputAddress = input_address
        self.amount = amount
        self.meta_data = meta_data if meta_data else ""
        self.hash = UniHelpers.hash(self.__dict__)
