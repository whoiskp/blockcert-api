import json
import uuid
from uni_helpers import UniHelpers


class UniBlock(object):
    def __init__(self, index, timestamp, proof, transactions, prev_hash_block):
        self.index = index
        self.time = timestamp
        self.proof = proof
        # convert list UniTracsaction to json for hash
        self.tx = [trans.__dict__ for trans in transactions]
        self.prev_block = prev_hash_block
        self.mrkl_root = UniHelpers.get_merkle_root_trans(transactions)
        self.hash = UniHelpers.hash(self.__dict__)


# Test Uni Block
if __name__ == '__main__':
    trans = [{
        "time": 123541263,
        "inputAddress": "aksdfhkajsdfka",
        "outputAddress": "aksdfhkajsdfka",
        "amount": 1,
        "meta_data": ""},
        {
            "time": 123,
            "inputAddress": "aksdfhkajsdfka",
            "outputAddress": "aksdfhkajsdfka",
            "amount": 1,
            "meta_data": ""
        },
        {
            "time": 454543,
            "inputAddress": "aksdfhkajsdfka",
            "outputAddress": "aksdfhkajsdfka",
            "amount": 1,
            "meta_data": ""
        },
        {
            "time": 2134324,
            "inputAddress": "aksdfhkajsdfka",
            "outputAddress": "aksdfhkajsdfka",
            "amount": 1,
            "meta_data": ""
        },
        {
            "time": 234343,
            "inputAddress": "aksdfhkajsdfka",
            "outputAddress": "aksdfhkajsdfka",
            "amount": 1,
            "meta_data": ""
        }
    ]
    # uni = UniBlock(1, time(), 10, trans, "asdfsdf")
    # print(uni.hash)
    uid = str(uuid.uuid4())
    uid = uid.replace("-", "")
    # print(uid)
    dic = {'key1': ["value1", "value2"],
           'key2': ["value77", "something"]}
    res = "value77" in [x for v in dic.values() for x in v]
    print(res)
