import json
from time import time
from urllib.parse import urlparse
import requests

from .uni_block import UniBlock
from .uni_trans import UniTransaction
from uni_helpers import UniHelpers
from uni_config import FIRST_PROOF, NUM_OF_BLOCKS_TO_ADD, UNICOIN_NETWORK_ADDRESS


class UniNetwork(object):
    # in update version we will get in config.ini
    NODE_INDENTIFIER = UniHelpers.get_uuid()

    def __init__(self):
        self.chain = []
        self.current_transactions = []
        self.nodes = []

        self.chain.append(self.__create_genesis_block())
        self.__gen_first_num_block()

    @property
    def last_block(self):
        # Returns the last Block in the chain
        return self.chain[-1]

    def __create_genesis_block(self):
        return UniBlock(1, time(), FIRST_PROOF, [], 0)

    def __gen_first_num_block(self):
        for i in range(0, NUM_OF_BLOCKS_TO_ADD):
            # get last proof of chain
            last_proof = self.last_block.proof

            # find next valid proof
            proof = UniHelpers.proof_of_work(last_proof)

            # Once we find a valid proof of work,
            # we know we can mine a block so
            # we reward the miner by adding a transaction
            self.new_transaction(UNICOIN_NETWORK_ADDRESS, self.NODE_INDENTIFIER, 1)

            block_to_add = self.new_block(proof)

            # Tell everyone about it!
            # print("Block #{} has been added to the blockchain!".format(block_to_add.index))
            # print("Hash: {}\n".format(block_to_add.hash))

        # print(f"chain length: {len(self.chain)}")

    def new_block(self, proof):
        """
        Create a new Block in the Blockchain

        :param proof: <int> The proof given by the Proof of Work algorithm
        :return: <dict> New Block
        """

        block = UniBlock(len(self.chain) + 1, time(), proof, self.current_transactions, self.last_block.hash)

        # Reset the current list of transactions
        self.current_transactions = []

        self.chain.append(block)

        return block

    def new_transaction(self, output_address, input_address, amount, meta_data=None):
        tx_info = UniTransaction(time(), output_address, input_address, amount, meta_data)

        self.current_transactions.append(tx_info)

        return tx_info.hash

    @staticmethod
    def new_unicoin_address():
        uid = UniHelpers.get_uuid()
        return uid

    def list_nodes(self):
        return self.nodes

    def register_node(self, address):
        """
        Add a new node to the list of nodes

        :param address: Address of node. Ex: 'http://192.168.0.5:5000'
        :return:
        """

        parsed_url = urlparse(address)
        if parsed_url.netloc:
            self.nodes.append(parsed_url.netloc)
        elif parsed_url.path:
            # Accepts an URL without schema like '192.168.0.5:5000'
            self.nodes.append(parsed_url.path)
        else:
            raise ValueError('Invalid URL')

    def resolve_conflicts(self):
        """
        This is our Consensus Algorithm, it resolves conflicts
        by replacing out chain with the longest ont in the network

        :return: <bool> True if out chain was replaced, False if not
        """

        neighbours = self.nodes
        new_chain = None

        # We're only looking for chains longer than ours
        max_length = len(self.chain)

        # Grab and verify the chains from all the nodes in out net work
        for node in neighbours:
            response = requests.get(f'http://{node}/chain')

            if response.status_code == 200:
                length = response.json()['length']
                chain = response.json()['chain']

                # Check if the length is longer and the chain is valid
                if length > max_length and self.valid_chain(chain):
                    max_length = length
                    new_chain = chain

        if new_chain:
            chain_block = []
            for c in new_chain:
                chain_block.append(UniBlock(c['index'], c['time'], c['proof'], [
                    UniTransaction(t["time"], t["outputAddress"], t["inputAddress"], t["amount"], t["meta_data"]) for
                    t in c['tx']], c['prev_block']))
            print(chain_block)
            self.chain = chain_block
            return True

        return False

    def get_tx(self, txid):
        for block in self.chain:
            for tx in block.tx:
                if str(tx['hash']) == txid:
                    return tx
        return None

    def valid_chain(self, chain):
        """
        Determine if a given blockchain is valid
        :param chain: <list> A blockchain
        :return: <bool> True if valid, False if not
        """

        last_block = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]
            # Check that the hash of the block is correct
            if block['prev_block'] != last_block['hash']:
                return False

            # Check that the Proof of Work is correct
            if not UniHelpers.valid_proof(last_block['proof'], block['proof']):
                return False

            last_block = block
            current_index += 1

        return True


if __name__ == '__main__':
    blockchain = UniNetwork()
