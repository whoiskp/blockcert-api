import json
import hmac
import hashlib
from merkletools import MerkleTools
import uuid


class UniHelpers:
    @staticmethod
    def get_uuid():
        return str(uuid.uuid4()).replace("-", "")

    @staticmethod
    def hash(data):
        """
        Create hash SHA-256 hash for data
        :param data: <dict> block or certificate
        :return: <str> hash value
        """

        # We must make sure that the Dictionary is Ordered, or we'll have inconsistent hashes
        data_str = json.dumps(data, sort_keys=True).encode()
        return hashlib.sha256(data_str).hexdigest()

    @staticmethod
    def get_merkle_root_trans(trans):
        if len(trans) <= 0:
            return 0
        mrkl_tree = MerkleTools(hash_type="sha256")
        for tx in trans:
            mrkl_tree.add_leaf(tx.hash)

        mrkl_tree.make_tree()
        return mrkl_tree.get_merkle_root()

    @staticmethod
    def valid_proof(last_proof, proof):
        """
        Validates the Proof: Does hash(last_proof, proof) contain 4 leading zeroes?

        :param last_proof: <int> Previous Proof
        :param proof: <int> Current Proof
        :return: <bool> True if correct, False if not.
        """

        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()

        return guess_hash[:4] == "0000"

    @staticmethod
    def proof_of_work(last_proof):
        proof = 0
        while UniHelpers.valid_proof(last_proof, proof) is False:
            proof += 1

        return proof

    @staticmethod
    def valid_chain(chain):
        """
        Determine if a given blockchain is valid

        :param chain: <list> A blockchain
        :return: <bool> True if valid, False if not
        """
        last_block = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]
            print(f'{last_block}')
            print(f'{block}')
            print("\n-------------------\n")

            # Check that the hash of the block is correct
            if block['prev_block'] != UniHelpers.hash(last_block):
                return False

            # Check that the Proof of Work is Correct
            if not UniHelpers.valid_proof(last_block['proof'], block['proof']):
                return False

            last_block = block
            current_index += 1

        return True

    @staticmethod
    def is_valid_request(private_key, base_string, authorization):
        print(private_key)
        print(base_string)
        hmac_key = hmac.new(bytes(private_key, 'utf-8'), msg=bytes(base_string, 'utf=-8'),
                            digestmod=hashlib.sha256).hexdigest()
        print(hmac_key)
        print(authorization)
        return str(hmac_key) == str(authorization)

    @staticmethod
    def get_private_key(public_key):
        return hashlib.sha256(public_key[:4].encode()).hexdigest()[:10]


    @staticmethod
    def valid_merkle_proof(proof, target_hash, merkle_root):
        mrkl_tree = MerkleTools(hash_type="sha256")
        return mrkl_tree.validate_proof(proof, target_hash, merkle_root)

