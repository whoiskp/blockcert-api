import requests
from pprint import pprint

POS_CHAIN_NODE = 'http://node-ico:3000/ico'
TO_ADDRESS = 'UNICERT ADDRESS'
CERT_AMOUNT = 2


class PosChain(object):
    @staticmethod
    def make_tx(metadata=''):
        raw = {
            'to': TO_ADDRESS,
            'amount': CERT_AMOUNT,
            'type': 'TRANSACTION',
            'meta': metadata
        }

        res = requests.post(f'{POS_CHAIN_NODE}/transact', json=raw)

        if res.status_code != 500:
            print(res.json(), flush=True)
            return res.json()['txid']

        return False

    @staticmethod
    def get_tx(txid):
        res = requests.get(f'{POS_CHAIN_NODE}/tx/{txid}')
        print(txid, flush=True)
        if res.status_code != 500:
            return res.json()

        return False
