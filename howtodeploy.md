# HOW TO DEPLOY

## SETUP PROJECT
1. Proof Of Stake - Blockchain network
    - node version: `12.4.0`
    - npm version: `6.9.0`
    - git version `2.23.0`
    - link project: 
    ```sh
    > git clone https://paper:iV1y7sjMN_b695zRrKWC@gitlab.com/ptithcm/pos-chain.git
    ```
    - init pos-network: **check `README.md` file**

2. Blockcert api
    - python: `3.6`
    - link project:
    ```sh
    > git clone https://paper:zH9Qx97ksrYmthMkXRVZ@gitlab.com/whoiskp/blockcert-api.git 
    ```
    - command run: 
    ```
    > python uni_coin.py
    ```

3. POS-UNICERT GUI
    - python: `3.6`
    - link project:
    ```sh
    > git clone https://paper:BjGKLVVTyec9MayjwFju@gitlab.com/ptithcm/pos-unicert-dashboard.git
    ```
    - command run: `> python manage.py run` or `> python3 manage.py run`


## How to pull code
1. ```> cd path_project```
2. ```> git pull origin master -u```

