import json
import hashlib
from flask import Flask, jsonify, request
from flask_cors import CORS
from merkletools import MerkleTools

from models import UniNetwork
from uni_helpers import UniHelpers
from uni_config import UNICOIN_NETWORK_ADDRESS, NUM_OF_BLOCKS_TO_ADD, FIRST_PROOF
from pos_chain import PosChain
# Init our Node
app = Flask(__name__)
CORS(app)
app.debug = True

# Init Unicoin blockchain
uniCoin = UniNetwork()


@app.route('/', methods=['GET'])
def show_indentifier():
    return uniCoin.NODE_INDENTIFIER


@app.route('/chain', methods=['GET'])
def full_chain():
    try:
        chain_json = [block.__dict__ for block in uniCoin.chain]
        response = {
            'chain': chain_json,
            'length': len(uniCoin.chain)
        }

        print('type class')

        return jsonify(response), 200
    except Exception:
        response = {
            'chain': uniCoin.chain,
            'length': len(uniCoin.chain)
        }

        print('type json')
        return jsonify(response), 200


@app.route('/mine', methods=['GET'])
def mine():
    last_block = uniCoin.last_block
    print(last_block)
    last_proof = last_block.proof

    proof = UniHelpers.proof_of_work(last_proof)

    uniCoin.new_transaction(UNICOIN_NETWORK_ADDRESS,
                            uniCoin.NODE_INDENTIFIER, 1)
    block = uniCoin.new_block(proof)

    response = {
        'message': "New Block Forged",
        'index': block.index,
        'transactions': block.tx,
        'proof': block.proof,
        'previous_hash': block.prev_block
    }

    print(response)
    return jsonify(response), 200


@app.route('/tx', methods=['POST'])
def new_transaction():
    values = request.get_json()
    url = request.base_url

    base_string = '{}|{}'.format(url, values).replace(' ', '', 7)
    public_key = values['outputAddress']
    prk_key = UniHelpers.get_private_key(public_key)
    authorization = request.headers.get('Authorization')

    print(values)
    print(base_string)
    print(public_key)
    print(prk_key)
    print(authorization)

    is_valid = UniHelpers.is_valid_request(prk_key, base_string, authorization)
    print(is_valid)

    if not is_valid:
        return 'Bad request', 401

    # Check that the required fields are in the POST'ed data
    required = ['outputAddress', 'inputAddress', 'amount', 'meta_data']

    if not all(k in values for k in required):
        return 'Missing values', 400

    # Create a new Transaction
    txid = uniCoin.new_transaction(
        values['outputAddress'],
        values['inputAddress'],
        values['amount'],
        values['meta_data']
    )

    for tx in uniCoin.current_transactions:
        print(tx.__dict__)

    response = {
        'message': f'Transaction will be added to Block {uniCoin.last_block.index + 1}', 'tx_id': txid}

    # Boardcast transaction for all node in network

    return jsonify(response), 201


@app.route('/tx/<txid>', methods=['GET'])
def get_transaction(txid):
    trans = uniCoin.get_tx(txid)
    if trans:
        return jsonify(trans), 200
    response = {'message': f'not found'}
    return jsonify(response), 404


@app.route('/address_wallet', methods=['GET'])
def get_address_wallet():
    pub_key = uniCoin.new_unicoin_address()
    # print(pub_key[:4].encode())
    prk_key = UniHelpers.get_private_key(pub_key)
    response = {'public_key': f'{pub_key}', 'private_key': prk_key}
    return jsonify(response), 200


@app.route('/nodes', methods=['GET'])
def get_list_nodes():
    return jsonify(uniCoin.list_nodes()), 200


@app.route('/nodes/register', methods=['POST'])
def register_nodes():
    nodes = request.get_json().get('nodes')
    if nodes is None:
        return "Error: Please supply a valid list of nodes", 400

    for node in nodes:
        uniCoin.register_node(node)

    response = {
        'message': 'New nodes have been added',
        'total_nodes': list(uniCoin.nodes),
    }

    return jsonify(response), 200


@app.route('/nodes/resolve', methods=['GET'])
def consensus():
    replaced = uniCoin.resolve_conflicts()
    chain_json = [block.__dict__ for block in uniCoin.chain]

    if replaced:
        response = {
            'message': 'Our chain was replaced',
            'new_chain': chain_json
        }
    else:
        response = {
            'message': 'Our chain is authoritative',
            'chain': chain_json
        }

    return jsonify(response), 200


@app.route('/gen_sign_cert', methods=['POST'])
def gen_sign_cert():
    cert_batch = request.get_json().get('data')
    pub_key = request.get_json().get('pub_key')
    prk_key = request.get_json().get('prk_key')

    def gen_cert(cert_batch, pub_key, prk_key):
        returned_data = []
        mrkl_tree = MerkleTools(hash_type="sha256")
        for cert in cert_batch:
            mrkl_tree.add_leaf(UniHelpers.hash(cert))

        mrkl_tree.make_tree()
        mrkl_root = mrkl_tree.get_merkle_root()

        prk_key_real = UniHelpers.get_private_key(pub_key)
        if not prk_key == prk_key_real:
            return None

        # Create a new Transaction
        txid = uniCoin.new_transaction(
            pub_key,
            'Unicoin Network',
            1,
            'UNI_CERT ' + mrkl_root
        )

        for i in range(0, len(cert_batch)):
            returned_data.append({
                'merkleRoot': mrkl_root,
                'targetHash': UniHelpers.hash(cert),
                'proof': mrkl_tree.get_proof(i),
                "anchors": [{
                    "txid": txid,
                    "type": "UniCert",
                    "chain": "UniCert"
                }]
            })

        return returned_data

    res = gen_cert(cert_batch, pub_key, prk_key)
    return jsonify({'data': res})


@app.route('/valid_cert', methods=['POST'])
def validate_uni_cert():
    cert = request.get_json().get('cert')

    def valid_cert(cert):
        unicertSignature = cert.pop('unicertSignature', None)

        res = {
            'hash': False,
            'proof': False,
            'meta_data': False
        }

        if not unicertSignature:
            return res

        print(cert)
        cert_hash = UniHelpers.hash(cert)
        print(cert_hash)
        if cert_hash == unicertSignature['targetHash']:
            res['hash'] = True

        if UniHelpers.valid_merkle_proof(unicertSignature['proof'], cert_hash, unicertSignature['merkleRoot']):
            res['proof'] = True

        txid = unicertSignature['anchors'][0]['txid']
        trans = uniCoin.get_tx(txid)

        if trans and trans['meta_data'] == 'UNI_CERT ' + unicertSignature['merkleRoot']:
            res['meta_data'] = True

        return res

    res = valid_cert(cert)
    return jsonify({'data': res})


@app.route('/pos/gen_sign_cert', methods=['POST'])
def pos_gen_sign_cert():
    cert_batch = request.get_json().get('data')

    def gen_cert(cert_batch):
        returned_data = []
        mrkl_tree = MerkleTools(hash_type="sha256")
        for cert in cert_batch:
            mrkl_tree.add_leaf(UniHelpers.hash(cert))

        mrkl_tree.make_tree()
        mrkl_root = mrkl_tree.get_merkle_root()

        # Create a new Transaction
        txid = PosChain.make_tx(f'UNI_CERT {mrkl_root}')

        for index, cert in enumerate(cert_batch):
            cert['unicertSignature'] = {
                'merkleRoot': mrkl_root,
                'targetHash': UniHelpers.hash(cert),
                'proof': mrkl_tree.get_proof(index),
                "anchors": [{
                    "txid": txid,
                    "type": "UniCert POS",
                    "chain": "UniCert POS"
                }]
            }

        return cert_batch

    res = gen_cert(cert_batch)
    return jsonify({'data': res})


@app.route('/pos/valid_cert', methods=['POST'])
def pos_validate_uni_cert():
    def valid_cert(cert):
        unicertSignature = cert.pop('unicertSignature', None)

        res = {
            'hash': False,
            'proof': False,
            'meta_data': False
        }

        if not unicertSignature:
            return res

        print(cert)
        cert_hash = UniHelpers.hash(cert)
        # print(cert_hash)
        if cert_hash == unicertSignature['targetHash']:
            res['hash'] = True

        if UniHelpers.valid_merkle_proof(unicertSignature['proof'], cert_hash, unicertSignature['merkleRoot']):
            res['proof'] = True

        txid = unicertSignature['anchors'][0]['txid']
        trans = PosChain.get_tx(txid)

        if trans and trans['meta'] == 'UNI_CERT ' + unicertSignature['merkleRoot']:
            res['meta_data'] = True

        return res

    cert = request.get_json().get('cert')
    print(cert, flush=True)
    res = valid_cert(cert)
    print(res, flush=True)
    return jsonify(res)


@app.route('/pos/tx')
def pos_make_tx():
    # PosChain.make_tx(metadata='UNICERT 123')
    PosChain.get_tx('ba7efc80-087d-11ea-9517-adddfb442504')
    return ""


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7001)
